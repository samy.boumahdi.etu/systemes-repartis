package sr;
/**
 * Author : Samy Boumahdi
 */
public class App {
    public static void main(String[] args){
        try {
            Connexion connexion;

            if (args.length == 0) {
                System.out.println("Veuillez donner en argument une adresse");
            }
            if (args.length > 3){
                System.out.println("Plus de 3 arguments ont été donnés");
            }
            else {
                connexion = new Connexion(args);
                connexion.Start();
            }
        } catch (Exception e) {
            System.out.println("Une erreur est surevenu veuillez vérifier que l'adresse du serveur est correct");
        }
    }
}