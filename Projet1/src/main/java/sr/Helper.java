package sr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Helper {

    /**
     *  Coupe la ligne en paramètre en retirant les parenthèses pour faciliter son utilisation
     * @param line la ligne à couper.
     * @return la line coupée
     */
    public static String CutString(String line) {
        line = line.substring(line.indexOf("(") + 1);
        line = line.substring(0, line.indexOf(")"));
        return line;
    }

    /**
     * Récupère une adresse IP à partir d'une line reçue, les 4 premiers nombres formeront la nouvelle adresse.
     * @param line la line contenant l'adresse IP
     * @return notre adresse IP
     */
    public static String GetIP(String line) {
        line = CutString(line);
        List<String> listForIP = Arrays.asList(line.split(","));
        listForIP = listForIP.subList(0,4);
        return String.join(".", listForIP);
    }

    /**
     * Recupère un nouveau port en additionnant les 2 derniers nombres de la line reçue.
     * @param line la line contenant le port
     * @return notre nouveau port
     */
    public static int GetPort(String line) {
        line = CutString(line);
        List<Integer> integerlistForPort= new ArrayList<Integer>();
        List<String> listForPort = Arrays.asList(line.split(","));
        listForPort = listForPort.subList(4, listForPort.size());
        for(String s : listForPort) integerlistForPort.add(Integer.valueOf(s));
        integerlistForPort.set(0, 256 * integerlistForPort.get(0));
        return integerlistForPort.stream().mapToInt(Integer::intValue).sum();
    }

    /**
     * Affiche les répertoires plus en profondeur
     * @param myList Une liste de répertoires
     */
    public static void PrintMyListInformation(List<Information> myList) {
        for (int i = 0; i < myList.size(); i++)
        {
            System.out.println("|     |    ├── " + myList.get(i).Name);
        }
    }
}