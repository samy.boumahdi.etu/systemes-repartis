package sr;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe permet de définir un répertoire sous forme d'objet.
 * Le name représente le nom du répertoire/fichier.
 * Le LstInfo est une liste qui représente tous les sous-dossiers du répertoire.
 */
public class Information {

    public String Name;
    public List<Information> LstInfo;

    Information(String name)
    {
        Name = name;
        LstInfo = new ArrayList<>();
    }
}
