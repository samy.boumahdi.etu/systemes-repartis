package sr;

import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Connexion {

    protected Socket socket;
    protected BufferedReader reader;
    protected PrintWriter writer;
    protected String line;

    /**
     * Connexion sur le serveur indiqué pour l'args[0]
     * @param args 1 serveur souhaité pour la connexion
     * @param args 2 (optionel) authentification avec un user
     * @param args 3 (optionel) authentification avec un password
     * @throws IOException
     */
    public Connexion(String[] args) throws IOException{
        this.socket = new Socket(args[0], 21);
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        receivedMessage();
        if (args.length == 3) {
            Authentification(args[1], args[2]);
        }
        else
            Authentification();
    }

    /**
     * Authentification anonyme
     * @throws IOException
     */
    public  void Authentification() throws IOException{
        sendMessage("USER anonymous");
        receivedMessage();
        sendMessage("PASS");
        receivedMessage();
    }

    /**
     * AUthentification avec des identifiants
     * @param user
     * @param password
     * @throws IOException
     */
    public void Authentification(String user, String password) throws IOException
    {
        sendMessage("USER " + user);
        receivedMessage();
        sendMessage("PASS " + password);
        receivedMessage();
    }

    /**
     * Envoie une commande au serveur
     * @param command la commande que l'on envoie au serveur
     * @throws IOException
     */
    public void sendMessage(String command) throws IOException {
        if (socket == null)
            throw new IOException("I'm not connected.");
        else {
            writer.println(command);
        }
    }

    /**
     * La réponse qu'envoie le serveur après la commande envoyée
     * @throws IOException
     */
    public void receivedMessage() throws IOException {
        this.line = reader.readLine();
    }

    /**
     * Début du processus et de l'affichage des répertoires
     * @throws IOException
     */
    public void Start() throws IOException {
        List<Information> lstMainDir = new ArrayList<>();
        sendMessage("PASV");
        receivedMessage();

        if (line.contains("227")) {
            lstMainDir = GetDirectories(Helper.GetIP(line), Helper.GetPort(line));
        }
        receivedMessage();

        for (Information dir : lstMainDir) {
            System.out.println("├──" + dir.Name);
            sendMessage("CWD " + "/" + dir.Name);
            receivedMessage();

            if (line != null && line.contains("250")) {
                writer.println("PASV");
                receivedMessage();

                if (line != null && line.contains("227")) {
                    List<Information> test = GetDirectories(Helper.GetIP(line), Helper.GetPort(line));
                    dir.LstInfo = test;

                    receivedMessage();
                    while (test != null) {
                        for (Information dir2 : dir.LstInfo) {
                            System.out.println("|     ├── " + dir2.Name);
                            sendMessage("CWD " + "/" + dir.Name + "/" + dir2.Name);
                            receivedMessage();
                            sendMessage("PASV");
                            receivedMessage();
                            if (line != null && line.contains("227")) {
                                test = GetDirectories(Helper.GetIP(line), Helper.GetPort(line));
                                if (test != null)
                                    Helper.PrintMyListInformation(test);
                                dir2.LstInfo = test;
                                dir.LstInfo = dir2.LstInfo;
                                receivedMessage();
                            }
                        }

                        test = null;
                    }
                }
            }
        }
    }

    /**
     * Renvoie une liste d'objets en réponse à la commande NLST
     * @param ip
     * @param port
     * @return une liste de d'éléments
     */
    public List<Information> GetDirectories(String ip, int port) {
        writer.println("NLST");
        List<Information> listDir = new ArrayList<>();
        Socket newSocket;
        try {
            newSocket = new Socket(ip,port);
            BufferedReader newReader = new BufferedReader(new InputStreamReader(newSocket.getInputStream()));

            String response;
            while((response = newReader.readLine())!=null){
                Information info = new Information(response);
                listDir.add(info);
            }

            receivedMessage();
            newSocket.close();
        }
        catch (IOException e) {
            System.out.println("problem à l'init de ma socket");
        }
        return listDir;
    }
}