package sr;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class HelperTest {

    @Test
    public void testCutString() {
        String line = "227 Entering Passive Mode (212,27,60,27,161,69).";
        String cutLine = Helper.CutString(line);
        assertTrue(cutLine.equals("212,27,60,27,161,69"));
    }


    @Test
    public void testGetIP() {
        String line = "227 Entering Passive Mode (212,27,60,27,161,69).";
        String ip = Helper.GetIP(line);
        assertTrue(ip.equals("212.27.60.27"));
    }


    @Test
    public void testGetPort() {
        String line = "227 Entering Passive Mode (212,27,60,27,161,69).";
        Integer port = Helper.GetPort(line);
        assertTrue(port == 41285);
    }
}
