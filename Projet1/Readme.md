# Projet 1 - Systèmes Répartis
	
	- Samy Boumahdi 

## Responsable
    
    - Arthur d'Azémar

## But du projet :

	- Se connecter à un serveur ftp distant et afficher l'intégralité de l'arborescence de ce serveur.
	- L'affichage s'inspirera de la commande tree de Linux.
  
# Pour lancer le projet :

	- Se placer dans le répertoire Projet1 et utiliser la commande mvn package

# Pour compiler le projet, 2 façons (toujours depuis le répertoire Projet1) :


**1) Avec 1 argument : connexion au serveur mis en argument**
	
	- java -jar target/Projet1-1.0-SNAPSHOT.jar <serveur>
	
**2) Avec 3 arguments : connexion au serveur mis en argument en s'identifiant avec un user et password**

	- java -jar target/Projet1-1.0-SNAPSHOT.jar <serveur> <user> <password>

# Problèmes rencontrés :

	- difficultés à explorer l'arborescence plus en profondeur.
